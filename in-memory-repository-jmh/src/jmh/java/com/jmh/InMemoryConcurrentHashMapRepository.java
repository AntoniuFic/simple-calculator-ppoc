package com.jmh;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryConcurrentHashMapRepository<T> implements Repository<T> {
    private final ConcurrentHashMap<T, Boolean> map;

    InMemoryConcurrentHashMapRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T item) {
        this.map.put(item, true);
    }

    @Override
    public void remove(T item) {
        this.map.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return this.map.containsKey(item);
    }
}

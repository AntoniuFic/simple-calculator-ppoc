package com.jmh;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

public class InMemoryArrayListRepositoryEclipse<T> implements Repository<T> {
    private final MutableList<T> list;

    InMemoryArrayListRepositoryEclipse() {
        list = Lists.mutable.empty();
    }

    @Override
    public void add(T item) {
        this.list.add(item);
    }

    @Override
    public void remove(T item) {
        this.list.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return this.list.contains(item);
    }
}

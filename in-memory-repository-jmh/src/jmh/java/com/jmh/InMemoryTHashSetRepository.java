package com.jmh;
import gnu.trove.THashSet;

public class InMemoryTHashSetRepository<T> implements Repository<T> {
    private THashSet<T> set;

    @Override
    public void add(T item) {
        this.set.add(item);
    }

    @Override
    public void remove(T item) {
        this.set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return this.set.contains(item);
    }
}

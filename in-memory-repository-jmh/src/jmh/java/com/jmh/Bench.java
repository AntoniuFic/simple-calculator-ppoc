package com.jmh;

import org.openjdk.jmh.annotations.*;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class Bench {
    public InMemoryArrayRepository<Integer> inMemoryArrayRepository;
    public InMemoryTHashSetRepository<Integer> inMemoryTHashSetRepository;
    public InMemoryArrayListRepositoryEclipse<Integer> inMemoryArrayListRepositoryEclipse;
    public InMemoryConcurrentHashMapRepository<Integer> inMemoryConcurrentHashMapRepository;
    public InMemoryHashSetRepository<Integer> inMemoryHashSetRepository;
    public InMemoryTreeSetRepository<Integer> inMemoryTreeSetRepository;

    @Setup(Level.Iteration)
    public void setup() {
        this.inMemoryArrayRepository = new InMemoryArrayRepository<>();
        this.inMemoryTHashSetRepository = new InMemoryTHashSetRepository<>();
        this.inMemoryArrayListRepositoryEclipse = new InMemoryArrayListRepositoryEclipse<>();
        this.inMemoryConcurrentHashMapRepository = new InMemoryConcurrentHashMapRepository<>();
        this.inMemoryHashSetRepository = new InMemoryHashSetRepository<>();
        this.inMemoryTreeSetRepository = new InMemoryTreeSetRepository<>();
    }

    @Benchmark
    public void inMemoryArrayRepositoryInsert() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryArrayRepository.add(i);
        }
    }

    @Benchmark
    public void inMemoryTHashSetRepositoryInsert() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryTHashSetRepository.add(i);
        }
    }

    @Benchmark
    public void inMemoryArrayListRepositoryEclipseInsert() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryArrayListRepositoryEclipse.add(i);
        }
    }

    @Benchmark
    public void inMemoryConcurrentHashMapRepositoryInsert() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryConcurrentHashMapRepository.add(i);
        }
    }

    @Benchmark
    public void inMemoryHashSetRepositoryInsert() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryHashSetRepository.add(i);
        }
    }

    @Benchmark
    public void inMemoryTreeSetRepositoryInsert() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryTreeSetRepository.add(i);
        }
    }

    @Benchmark
    public void inMemoryArrayRepositoryContains() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryArrayRepository.contains(i);
        }
    }

    @Benchmark
    public void inMemoryTHashSetRepositoryContains() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryTHashSetRepository.contains(i);
        }
    }

    @Benchmark
    public void inMemoryArrayListRepositoryEclipseContains() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryArrayListRepositoryEclipse.contains(i);
        }
    }

    @Benchmark
    public void inMemoryConcurrentHashMapRepositoryContains() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryConcurrentHashMapRepository.contains(i);
        }
    }

    @Benchmark
    public void inMemoryHashSetRepositoryContains() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryHashSetRepository.contains(i);
        }
    }

    @Benchmark
    public void inMemoryTreeSetRepositoryContains() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryTreeSetRepository.contains(i);
        }
    }

    @Benchmark
    public void inMemoryArrayRepositoryRemove() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryArrayRepository.remove(i);
        }
    }

    @Benchmark
    public void inMemoryTHashSetRepositoryRemove() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryTHashSetRepository.remove(i);
        }
    }

    @Benchmark
    public void inMemoryArrayListRepositoryEclipseRemove() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryArrayListRepositoryEclipse.remove(i);
        }
    }

    @Benchmark
    public void inMemoryConcurrentHashMapRepositoryRemove() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryConcurrentHashMapRepository.remove(i);
        }
    }

    @Benchmark
    public void inMemoryHashSetRepositoryRemove() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryHashSetRepository.remove(i);
        }
    }

    @Benchmark
    public void inMemoryTreeSetRepositoryRemove() {
        for (int i = 0; i < 1000; ++i) {
            this.inMemoryTreeSetRepository.remove(i);
        }
    }

}

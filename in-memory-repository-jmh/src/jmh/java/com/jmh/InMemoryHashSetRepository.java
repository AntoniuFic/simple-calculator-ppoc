package com.jmh;

import java.util.HashSet;

public class InMemoryHashSetRepository<T> implements Repository<T> {
    private final java.util.Set<T> set;

    InMemoryHashSetRepository() {
        this.set = new HashSet<>();
    }

    @Override
    public void add(T item) {
        this.set.add(item);
    }

    @Override
    public void remove(T item) {
        this.set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return this.set.contains(item);
    }
}

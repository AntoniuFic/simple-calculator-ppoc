package com.jmh;

import java.util.ArrayList;
import java.util.List;

public class InMemoryArrayRepository<T> implements Repository<T> {
    private final List<T> list;

    InMemoryArrayRepository() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T item) {
        this.list.add(item);
    }

    @Override
    public void remove(T item) {
        this.list.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return this.list.contains(item);
    }
}

package com.jmh;
import java.util.Set;
import java.util.TreeSet;

public class InMemoryTreeSetRepository<T> implements Repository<T> {
    private final Set<T> set;

    InMemoryTreeSetRepository() {
        this.set = new TreeSet<>();
    }

    @Override
    public void add(T item) {
        this.set.add(item);
    }

    @Override
    public void remove(T item) {
        this.set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return this.set.contains(item);
    }
}

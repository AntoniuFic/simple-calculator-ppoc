package com.jmh;

public interface Repository<T> {

    void add(T item);

    void remove(T item);

    boolean contains(T item);
}

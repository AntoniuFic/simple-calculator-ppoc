package org.example;

import java.util.ArrayList;
import java.util.List;

public class CalculatorDoublePrimitive implements Calculator<Double>{
    private final double[] numbers;

    public CalculatorDoublePrimitive(double[] numbers) {
        this.numbers = numbers;
    }

    @Override
    public Double sum() {
        double sum = 0.0;
        for (double number : this.numbers) {
            sum += number;
        }
        return sum;
    }

    @Override
    public Double average() {
        return this.sum() / this.numbers.length;
    }

    @Override
    public List<Double> top10Percent() {
        double[] sorted = this.numbers.clone();
        java.util.Arrays.sort(sorted);
        int skip = (int) (this.numbers.length * 0.9);
        Double[] result = new Double[skip];
        for (int i = 0; i < skip; i++) {
            result[i] = sorted[i];
        }
        return java.util.Arrays.asList(result);
    }
}

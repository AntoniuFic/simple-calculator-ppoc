package org.example;

import java.util.List;

public interface Calculator<T> {
    T sum();
    T average();

    List<T> top10Percent();
}

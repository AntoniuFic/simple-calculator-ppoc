package org.example;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class CalculatorBigDecimal implements Calculator<BigDecimal> {
    private final List<BigDecimal> numbers;

    public CalculatorBigDecimal(List<BigDecimal> numbers) {
        this.numbers = numbers;
    }

    @Override
    public BigDecimal sum() {
        return this.numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public BigDecimal average() {
        return this.sum().divide(BigDecimal.valueOf(this.numbers.size()));
    }

    @Override
    public List<BigDecimal> top10Percent() {
        return this.numbers.stream().sorted().skip((long) (this.numbers.size() * 0.9)).collect(Collectors.toList());
    }
}

package org.example;

import java.util.List;
import java.util.stream.Collectors;

public class CalculatorDouble implements Calculator<Double> {
    private final List<Double> numbers;

    public CalculatorDouble(List<Double> numbers) {
        this.numbers = numbers;
    }

    @Override
    public Double sum() {
        return this.numbers.stream().reduce(0.0, Double::sum);
    }

    @Override
    public Double average() {
        return this.sum() / this.numbers.size();
    }

    @Override
    public List<Double> top10Percent() {
        return this.numbers.stream().sorted().skip((long) (this.numbers.size() * 0.9)).collect(Collectors.toList());
    }
}

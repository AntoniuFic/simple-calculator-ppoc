package com.example;

import org.example.CalculatorBigDecimal;
import org.example.CalculatorDouble;
import org.example.CalculatorDoublePrimitive;
import org.openjdk.jmh.annotations.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class Bench {

    private CalculatorBigDecimal calculatorBigDecimal;
    private CalculatorDouble calculatorDouble;
    private CalculatorDoublePrimitive calculatorDoublePrimitive;

    @Setup(Level.Iteration)
    public void setup() {
        List<BigDecimal> bigDecimals = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        double[] doublePrimitives = new double[100000000];

        for (int i = 0; i < 100000000; i++) {
            bigDecimals.add(BigDecimal.valueOf(i));
            doubles.add((double) i);
            doublePrimitives[i] = i;
        }

        this.calculatorBigDecimal = new CalculatorBigDecimal(bigDecimals);
        this.calculatorDouble = new CalculatorDouble(doubles);
        this.calculatorDoublePrimitive = new CalculatorDoublePrimitive(doublePrimitives);
    }

    @Benchmark
    public void bigDecimalStreamSum() {
        BigDecimal sum = this.calculatorBigDecimal.sum();
        System.out.println(sum);
    }

    @Benchmark
    public void doubleObjectStreamSum() {
        Double sum = this.calculatorDouble.sum();
        System.out.println(sum);
    }

    @Benchmark
    public void doublePrimitiveStreamSum() {
        Double sum = this.calculatorDoublePrimitive.sum();
        System.out.println(sum);
    }

    @Benchmark
    public void bigDecimalStreamAverage() {
        BigDecimal average = this.calculatorBigDecimal.average();
        System.out.println(average);
    }

    @Benchmark
    public void doubleObjectStreamAverage() {
        Double average = this.calculatorDouble.average();
        System.out.println(average);
    }

    @Benchmark
    public void doublePrimitiveStreamAverage() {
        Double average = this.calculatorDoublePrimitive.average();
        System.out.println(average);
    }

    @Benchmark
    public void bigDecimalStreamTop10Percent() {
        List<BigDecimal> top10Percent = this.calculatorBigDecimal.top10Percent();
        System.out.println(top10Percent.size());
    }

    @Benchmark
    public void doubleObjectStreamTop10Percent() {
        List<Double> top10Percent = this.calculatorDouble.top10Percent();
        System.out.println(top10Percent.size());
    }

    @Benchmark
    public void doublePrimitiveStreamTop10Percent() {
        List<Double> top10Percent = this.calculatorDoublePrimitive.top10Percent();
        System.out.println(top10Percent.size());
    }
}

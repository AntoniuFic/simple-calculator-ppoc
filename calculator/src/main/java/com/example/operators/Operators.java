package com.example.operators;

public enum Operators {
    ADDITION("+"), DIVISION("/"), MULTIPLICATION("*"), SUBTRACTION("-");

    final String symbol;

    Operators(String symbol) {
        this.symbol = symbol;
    }
}

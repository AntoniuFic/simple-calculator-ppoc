package com.example.operators;

public class Addition implements IOperator {
    private static final Addition instance = new Addition();

    private Addition() {
    }

    public static Addition getInstance() {
        return instance;
    }
    @Override
    public Integer execute(Integer first, Integer second) {
        return first + second;
    }

    @Override
    public Double execute(Double first, Double second) {
        return first + second;
    }

    @Override
    public Float execute(Float first, Float second) {
        return first + second;
    }

    @Override
    public Long execute(Long first, Long second) {
        return first + second;
    }
}

package com.example.operators;

public class Multiplication implements IOperator {
    private static final Multiplication instance = new Multiplication();

    private Multiplication() {
    }

    public static Multiplication getInstance() {
        return instance;
    }
    @Override
    public Integer execute(Integer first, Integer second) {
        return first * second;
    }

    @Override
    public Double execute(Double first, Double second) {
        return first * second;
    }

    @Override
    public Float execute(Float first, Float second) {
        return first * second;
    }

    @Override
    public Long execute(Long first, Long second) {
        return first * second;
    }
}

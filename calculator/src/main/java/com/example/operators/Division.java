package com.example.operators;

public class Division implements IOperator {
    private static final Division instance = new Division();

    private Division() {
    }

    public static Division getInstance() {
        return instance;
    }

    @Override
    public Integer execute(Integer first, Integer second) {
        if (second == 0) {
            throw new ArithmeticException("Division by zero");
        }
        return first / second;
    }

    @Override
    public Double execute(Double first, Double second) {
        if (second == 0) {
            throw new ArithmeticException("Division by zero");
        }
        return first / second;
    }

    @Override
    public Float execute(Float first, Float second) {
        if (second == 0) {
            throw new ArithmeticException("Division by zero");
        }
        return first / second;
    }

    @Override
    public Long execute(Long first, Long second) {
        if (second == 0) {
            throw new ArithmeticException("Division by zero");
        }
        return first / second;
    }
}

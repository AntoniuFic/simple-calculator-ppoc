package com.example.operators;

public interface IOperator {
    Integer execute(Integer first, Integer second);

    Double execute(Double first, Double second);

    Float execute(Float first, Float second);

    Long execute(Long first, Long second);
}

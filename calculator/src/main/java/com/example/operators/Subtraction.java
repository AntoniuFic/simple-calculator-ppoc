package com.example.operators;

public class Subtraction implements IOperator {
    private static final Subtraction instance = new Subtraction();

    private Subtraction() {
    }

    public static Subtraction getInstance() {
        return instance;
    }
    @Override
    public Integer execute(Integer first, Integer second) {
        return first - second;
    }

    @Override
    public Double execute(Double first, Double second) {
        return first - second;
    }

    @Override
    public Float execute(Float first, Float second) {
        return first - second;
    }

    @Override
    public Long execute(Long first, Long second) {
        return first - second;
    }
}

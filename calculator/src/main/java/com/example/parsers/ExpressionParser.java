package com.example.parsers;

import com.example.operators.Addition;
import com.example.operators.Division;
import com.example.operators.Multiplication;
import com.example.operators.Subtraction;

public class ExpressionParser {
    private static final ExpressionParser instance = new ExpressionParser();

    private ExpressionParser() {
    }

    public static ExpressionParser getInstance() {
        return instance;
    }

    public String[] parse(String expression) throws Exception {
        String[] operandsAndOperators = expression.split(" ");
        if (operandsAndOperators.length != 3) {
            throw new Exception("Invalid expression");
        }

        return operandsAndOperators;
    }

    public Double getResult(String expression) throws Exception {
        String[] operandsAndOperators = parse(expression);
        double operand1 = Double.parseDouble(operandsAndOperators[0]);
        double operand2 = Double.parseDouble(operandsAndOperators[2]);
        char operator = operandsAndOperators[1].charAt(0);
        double result;

        switch (operator) {
            case '+':
                result = Addition.getInstance().execute(operand1, operand2);
                break;
            case '-':
                result = Subtraction.getInstance().execute(operand1, operand2);
                break;
            case '*':
                result = Multiplication.getInstance().execute(operand1, operand2);
                break;
            case '/':
                result = Division.getInstance().execute(operand1, operand2);
                break;
            default:
                throw new Exception("Invalid operator");
        }

        return result;
    }
}

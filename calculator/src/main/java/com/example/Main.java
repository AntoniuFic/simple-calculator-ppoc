package com.example;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.example.operators.*;
import com.example.parsers.ExpressionParser;

public class Main {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        try {
            String expression = reader.readLine();
            double result = ExpressionParser.getInstance().getResult(expression);
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
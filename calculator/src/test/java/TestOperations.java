import com.example.operators.Addition;
import com.example.operators.Division;
import com.example.operators.Multiplication;
import com.example.operators.Subtraction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestOperations {
    @Test
    public void testAddition() {
        Integer a = 1;
        Integer b = 2;

        Double c = 1.0;
        Double d = 2.0;

        Float e = 1.0f;
        Float f = 2.0f;

        Long g = 1L;
        Long h = 2L;

        int result_int = Addition.getInstance().execute(a, b);
        double result_double = Addition.getInstance().execute(c, d);
        float result_float = Addition.getInstance().execute(e, f);
        long result_long = Addition.getInstance().execute(g, h);
        Assertions.assertEquals(3, result_int, 0);
        Assertions.assertEquals(3.0, result_double, 0);
        Assertions.assertEquals(3f, result_float, 0);
        Assertions.assertEquals(3, result_long, 0);
    }

    @Test
    public void testSubtraction() {
        Integer a = 5;
        Integer b = 2;

        Double c = 5.0;
        Double d = 2.0;

        Float e = 5.0f;
        Float f = 2.0f;

        Long g = 5L;
        Long h = 2L;

        int result_int = Subtraction.getInstance().execute(a, b);
        double result_double = Subtraction.getInstance().execute(c, d);
        float result_float = Subtraction.getInstance().execute(e, f);
        long result_long = Subtraction.getInstance().execute(g, h);
        Assertions.assertEquals(3, result_int, 0);
        Assertions.assertEquals(3.0, result_double, 0);
        Assertions.assertEquals(3f, result_float, 0);
        Assertions.assertEquals(3, result_long, 0);
    }

    @Test
    public void testMultiplication() {
        Integer a = 5;
        Integer b = 2;

        Double c = 1.5;
        Double d = 2.0;

        Float e = 1.5f;
        Float f = 2.0f;

        Long g = 5L;
        Long h = 2L;

        int result_int = Multiplication.getInstance().execute(a, b);
        double result_double = Multiplication.getInstance().execute(c, d);
        float result_float = Multiplication.getInstance().execute(e, f);
        long result_long = Multiplication.getInstance().execute(g, h);
        Assertions.assertEquals(10, result_int, 0);
        Assertions.assertEquals(3.0, result_double, 0);
        Assertions.assertEquals(3f, result_float, 0);
        Assertions.assertEquals(10, result_long, 0);
    }

    @Test
    public void testDivision() {
        Integer a = 5;
        Integer b = 2;

        Double c = 5.0;
        Double d = 2.0;

        Float e = 5.0f;
        Float f = 2.0f;

        Long g = 5L;
        Long h = 2L;

        int result_int = Division.getInstance().execute(a, b);
        double result_double = Division.getInstance().execute(c, d);
        float result_float = Division.getInstance().execute(e, f);
        long result_long = Division.getInstance().execute(g, h);
        Assertions.assertEquals(2, result_int, 0);
        Assertions.assertEquals(2.5, result_double, 0);
        Assertions.assertEquals(2.5f, result_float, 0);
        Assertions.assertEquals(2, result_long, 0);

        // Test division by zero

        Integer a1 = 5;
        Integer b1 = 0;

        Double c1 = 5.0;
        Double d1 = 0.0;

        Float e1 = 5.0f;
        Float f1 = 0.0f;

        Long g1 = 5L;
        Long h1 = 0L;

        try{
            Division.getInstance().execute(a1, b1);
        } catch (Exception e2) {
            Assertions.assertEquals("Division by zero", e2.getMessage());
        }

        try{
            Division.getInstance().execute(c1, d1);
        } catch (Exception e2) {
            Assertions.assertEquals("Division by zero", e2.getMessage());
        }

        try{
            Division.getInstance().execute(e1, f1);
        } catch (Exception e2) {
            Assertions.assertEquals("Division by zero", e2.getMessage());
        }

        try{
            Division.getInstance().execute(g1, h1);
        } catch (Exception e2) {
            Assertions.assertEquals("Division by zero", e2.getMessage());
        }
    }
}

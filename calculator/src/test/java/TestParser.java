import com.example.parsers.ExpressionParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestParser {
    @Test
    public void testParseAddition() throws Exception {
        String expression = "1 + 2";
        String[] operandsAndOperators = ExpressionParser.getInstance().parse(expression);
        Assertions.assertEquals(3, operandsAndOperators.length);
        Assertions.assertEquals("1", operandsAndOperators[0]);
        Assertions.assertEquals("+", operandsAndOperators[1]);
        Assertions.assertEquals("2", operandsAndOperators[2]);
        Assertions.assertEquals(3, ExpressionParser.getInstance().getResult(expression), 0);
    }

    @Test
    public void testParseSubtraction() throws Exception {
        String expression = "5 - 3";
        String[] operandsAndOperators = ExpressionParser.getInstance().parse(expression);
        Assertions.assertEquals(3, operandsAndOperators.length);
        Assertions.assertEquals("5", operandsAndOperators[0]);
        Assertions.assertEquals("-", operandsAndOperators[1]);
        Assertions.assertEquals("3", operandsAndOperators[2]);
        Assertions.assertEquals(2, ExpressionParser.getInstance().getResult(expression), 0);
    }

    @Test
    public void testParseMultiplication() throws Exception {
        String expression = "5 * 3";
        String[] operandsAndOperators = ExpressionParser.getInstance().parse(expression);
        Assertions.assertEquals(3, operandsAndOperators.length);
        Assertions.assertEquals("5", operandsAndOperators[0]);
        Assertions.assertEquals("*", operandsAndOperators[1]);
        Assertions.assertEquals("3", operandsAndOperators[2]);
        Assertions.assertEquals(15, ExpressionParser.getInstance().getResult(expression), 0);
    }

    @Test
    public void testParseDivision() throws Exception {
        String expression = "6 / 3";
        String[] operandsAndOperators = ExpressionParser.getInstance().parse(expression);
        Assertions.assertEquals(3, operandsAndOperators.length);
        Assertions.assertEquals("6", operandsAndOperators[0]);
        Assertions.assertEquals("/", operandsAndOperators[1]);
        Assertions.assertEquals("3", operandsAndOperators[2]);
        Assertions.assertEquals(2, ExpressionParser.getInstance().getResult(expression), 0);
    }
}

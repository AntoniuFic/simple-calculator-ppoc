package org.example.comms.messages;

import java.io.Serializable;

public abstract class Packet implements Serializable {
    private final int id;

    protected final String sender;
    protected final String destination;

    protected Packet(int id, String sender, String destination) {
        this.id = id;
        this.sender = sender;
        this.destination = destination;
    }

    public int getId() {
        return id;
    };

    public String getSender() {
        return sender;
    }
    public String getDestination() {
        return destination;
    }
}

package org.example.comms.messages;

public class Message extends Packet {
    private final String content;

    public Message(int id, String sender, String destination, String content) {
        super(id, sender, destination);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return sender + ": " + content + "\n";
    }
}

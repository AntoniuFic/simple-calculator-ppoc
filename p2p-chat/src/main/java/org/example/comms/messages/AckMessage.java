package org.example.comms.messages;

public class AckMessage extends Packet {
    public AckMessage(int id, String sender, String destination) {
        super(id, sender, destination);
    }
}

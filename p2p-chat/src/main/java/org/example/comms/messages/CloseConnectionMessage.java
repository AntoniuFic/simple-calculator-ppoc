package org.example.comms.messages;

public class CloseConnectionMessage extends Packet {

    public CloseConnectionMessage(int id, String sender, String destination) {
        super(id, sender, destination);
    }
}

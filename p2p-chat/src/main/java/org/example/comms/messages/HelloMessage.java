package org.example.comms.messages;

public class HelloMessage extends Packet {
    public HelloMessage(int id, String sender, String destination) {
        super(id, sender, destination);
    }
}

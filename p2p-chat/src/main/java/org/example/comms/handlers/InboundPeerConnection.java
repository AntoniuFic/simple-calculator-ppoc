package org.example.comms.handlers;

import org.example.comms.messages.Packet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class InboundPeerConnection {
    private final Socket socket;
    private final ObjectInputStream objectInputStream;

    private final String sender;

    public InboundPeerConnection(Socket socket, int timeout, String sender) throws IOException {
        this.socket = socket;
        this.socket.setSoTimeout(timeout);
        this.objectInputStream = new ObjectInputStream(socket.getInputStream());
        this.sender = sender;
    }

    public Packet receiveMessage() throws IOException, ClassNotFoundException {
        return (Packet) objectInputStream.readObject();
    }

    public String getSender() {
        return sender;
    }

    public Socket getSocket() {
        return socket;
    }
}

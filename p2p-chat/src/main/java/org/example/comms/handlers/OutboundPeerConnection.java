package org.example.comms.handlers;

import org.example.comms.messages.Packet;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class OutboundPeerConnection {
    private final Socket socket;
    private final ObjectOutputStream objectOutputStream;

    private final String destination;

    public OutboundPeerConnection(Socket socket, String destination) throws IOException {
        this.socket = socket;
        this.objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        this.destination = destination;
    }

    public void sendMessage(Packet packet) throws IOException {
        objectOutputStream.writeObject(packet);
    }

    public String getDestination() {
        return destination;
    }

    public Socket getSocket() {
        return socket;
    }
}

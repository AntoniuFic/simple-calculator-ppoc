package org.example.comms.handlers;

import org.example.comms.messages.CloseConnectionMessage;
import org.example.comms.messages.Packet;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import static java.lang.Thread.sleep;

public class SenderThread implements Runnable {
    private final BlockingQueue<Packet> messageQueue;

    private final List<OutboundPeerConnection> outboudPeerConnections;

    private final List<Integer> distributedMessageIds;

    public SenderThread(BlockingQueue<Packet> messageQueue) {
        this.messageQueue = messageQueue;
        this.outboudPeerConnections = new ArrayList<>();
        this.distributedMessageIds = new ArrayList<>();
    }
    @Override
    public void run() {
        while (true) {
            try {
                Packet message = messageQueue.take();
                if (!distributedMessageIds.contains(message.getId())) {
                    List<OutboundPeerConnection> toSendTo = new ArrayList<>();
                    synchronized(outboudPeerConnections) {
                        for (OutboundPeerConnection outboudPeerConnection : outboudPeerConnections) {
                            if (outboudPeerConnection.getDestination().equals(message.getDestination())) {
                                outboudPeerConnection.sendMessage(message);
                                toSendTo.clear();

                                if (message instanceof CloseConnectionMessage) {
                                    sleep(500);
                                    outboudPeerConnection.getSocket().close();
                                    outboudPeerConnections.remove(outboudPeerConnection);
                                }
                                break;
                            } else {
                                toSendTo.add(outboudPeerConnection);
                            }
                        }
                    }
                    if (!toSendTo.isEmpty()) {
                        for (OutboundPeerConnection outboudPeerConnection : toSendTo) {
                            outboudPeerConnection.sendMessage(message);
                        }
                    }
                    distributedMessageIds.add(message.getId());
                }
                sleep(100);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void addOutboundPeerConnection(OutboundPeerConnection outboudPeerConnection) {
        synchronized(outboudPeerConnections) {
            outboudPeerConnections.add(outboudPeerConnection);
        }
    }

    public Socket removeOutboundPeerConnection(String destination) {
        Socket socket = null;
        synchronized(outboudPeerConnections) {
            for (OutboundPeerConnection outboudPeerConnection : outboudPeerConnections) {
                if (outboudPeerConnection.getDestination().equals(destination)) {
                    outboudPeerConnections.remove(outboudPeerConnection);
                    socket = outboudPeerConnection.getSocket();
                    break;
                }
            }
        }
        return socket;
    }
}

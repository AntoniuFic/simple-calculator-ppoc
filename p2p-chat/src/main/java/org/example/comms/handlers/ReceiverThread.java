package org.example.comms.handlers;

import org.example.comms.messages.Packet;

import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import static java.lang.Thread.sleep;

public class ReceiverThread implements Runnable {
    private final List<InboundPeerConnection> inboundPeerConnections;

    private final BlockingQueue<Packet> messageQueue;

    private final List<Integer> receivedMessageIds;

    public ReceiverThread(BlockingQueue<Packet> messageQueue) {
        this.inboundPeerConnections = new ArrayList<>();
        this.messageQueue = messageQueue;
        this.receivedMessageIds = new ArrayList<>();
    }

    @Override
    public void run() {
        while (true) {
            try {
                synchronized (inboundPeerConnections) {
                    for (InboundPeerConnection inboundPeerConnection : inboundPeerConnections) {
                        try {
                            Packet message = inboundPeerConnection.receiveMessage();

                            if (!receivedMessageIds.contains(message.getId())) {
                                messageQueue.put(message);
                                receivedMessageIds.add(message.getId());
                            }
                        } catch (SocketTimeoutException | SocketException e){
                            continue;
                        }
                    }
                }
                sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addInboundPeerConnection(InboundPeerConnection inboundPeerConnection) {
        synchronized (inboundPeerConnections) {
            inboundPeerConnections.add(inboundPeerConnection);
        }
    }

    public Socket removeInboundPeerConnection(String sender) {
        Socket socket = null;
        synchronized (inboundPeerConnections) {
            for (InboundPeerConnection inboundPeerConnection : inboundPeerConnections) {
                if (inboundPeerConnection.getSender().equals(sender)) {
                    inboundPeerConnections.remove(inboundPeerConnection);
                    socket = inboundPeerConnection.getSocket();
                    break;
                }
            }
        }
        return socket;
    }
}

package org.example.comms.handlers;

import org.example.comms.messages.AckMessage;
import org.example.comms.messages.HelloMessage;
import org.example.comms.messages.Packet;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class NewConnectionListener implements Runnable {
    private final String username;
    private final ReceiverThread receiverThread;
    private final SenderThread senderThread;

    public NewConnectionListener(String username, ReceiverThread receiverThread, SenderThread senderThread) {
        this.username = username;
        this.receiverThread = receiverThread;
        this.senderThread = senderThread;
    }

    @Override
    public void run() {
        int serverPort = 10023;
        while (serverPort < 10045) {
            try {
                ServerSocket serverSocket = new ServerSocket(serverPort);
                System.out.println("Listening on port " + serverPort + "...");
                while (true) {
                    Socket s = serverSocket.accept();
                    InboundPeerConnection inboundPeerConnection = new InboundPeerConnection(s, 700, "*");
                    Packet p = inboundPeerConnection.receiveMessage();
                    if (p instanceof HelloMessage) {
                        String sender = p.getSender();
                        String destination = p.getDestination();
                        if (destination.equals(username)) {
                            System.out.println("Received hello message from " + sender);
                            System.out.println("Sending ack message to " + sender);

                            OutboundPeerConnection outboundPeerConnection = new OutboundPeerConnection(s, sender);
                            outboundPeerConnection.sendMessage(new AckMessage((int) (System.currentTimeMillis() & 0xfffffff), username, sender));

                            receiverThread.addInboundPeerConnection(inboundPeerConnection);
                            senderThread.addOutboundPeerConnection(outboundPeerConnection);
                        } else {
                            System.out.println("Received hello message from " + sender + " but destination was " + destination);
                            System.out.println("Closing connection...");
                            s.close();
                        }
                    } else {
                        System.out.println("Unknown packet type received from " + p.getSender());
                        System.out.println("Closing connection...");
                        s.close();
                    }
                }
            } catch (IOException e) {
                System.out.println("Failed to create server socket!");
                serverPort++;
                if (serverPort > 10030) {
                    System.out.println("Failed to create server socket! Exiting...");
                    e.printStackTrace();
                    System.exit(1);
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

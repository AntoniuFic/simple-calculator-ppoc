package org.example.comms.handlers;

import org.example.comms.messages.CloseConnectionMessage;
import org.example.comms.messages.Message;
import org.example.comms.messages.Packet;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class MessageManagerThread implements Runnable {
    private final BlockingQueue<Packet> inbound;
    private final BlockingQueue<Packet> outbound;

    private final String username;

    private final ReceiverThread receiverThread;
    private final SenderThread senderThread;

    public MessageManagerThread(BlockingQueue<Packet> inbound, BlockingQueue<Packet> outbound, String username, ReceiverThread receiverThread, SenderThread senderThread) {
        this.inbound = inbound;
        this.outbound = outbound;
        this.username = username;
        this.receiverThread = receiverThread;
        this.senderThread = senderThread;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Packet packet = inbound.take();
                if (!packet.getSender().equals(username) && (packet.getDestination().equals(username) || packet.getDestination().equals("*"))) {
                    if (packet instanceof Message) {
                        System.out.println(packet.getSender() + ": " + ((Message) packet).getContent());
                    } else if (packet instanceof CloseConnectionMessage) {
                        System.out.println("Connection closed by " + packet.getSender());
                        Socket ssocket = senderThread.removeOutboundPeerConnection(packet.getSender());
                        Socket rsocket = receiverThread.removeInboundPeerConnection(packet.getSender());
                        if (ssocket != null) {
                            ssocket.close();
                        }
                        if (rsocket != null && rsocket != ssocket) {
                            rsocket.close();
                        }
                    } else {
                        System.out.println("Unknown packet type received from " + packet.getSender());
                    }
                }
                if (!packet.getDestination().equals(username)){
                    outbound.put(packet);
                }
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}

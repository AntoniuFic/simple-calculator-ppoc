package org.example;

import org.example.comms.handlers.InboundPeerConnection;
import org.example.comms.handlers.OutboundPeerConnection;
import org.example.comms.handlers.ReceiverThread;
import org.example.comms.handlers.SenderThread;
import org.example.comms.messages.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.BlockingQueue;

public class ConsoleInterface {
    private final BlockingQueue<Packet> outbound;
    private final ReceiverThread receiverThread;
    private final SenderThread senderThread;
    private final String username;

    private final BufferedReader console;

    public ConsoleInterface(BlockingQueue<Packet> outbound, ReceiverThread receiverThread, SenderThread senderThread, String username, BufferedReader console) {
        this.outbound = outbound;
        this.receiverThread = receiverThread;
        this.senderThread = senderThread;
        this.username = username;
        this.console = console;
    }


    public void start() {
        System.out.println("Welcome to the chat, " + username + "!");
        System.out.println("Type a message and press enter to send it to all connected peers.");
        System.out.println("Type /connect <ip> <port> <username> to connect to a peer.");
        System.out.println("Type /disconnect <username> to disconnect from a peer.");
        System.out.println("Type /to <username> to send a message to a peer.");
        System.out.println("Type /exit to exit the chat.");
        System.out.println("Type /help to see this message again.");
        while (true) {
            try {
                String input = console.readLine();
                if (input.startsWith("/connect ")) {
                    String[] parts = input.split(" ");
                    if (parts.length != 4) {
                        System.out.println("Invalid command. Type /help for help.");
                        continue;
                    }

                    Socket s = new Socket(parts[1], Integer.parseInt(parts[2]));
                    OutboundPeerConnection outboundPeerConnection = new OutboundPeerConnection(s, parts[3]);
                    outboundPeerConnection.sendMessage(new HelloMessage((int) (System.currentTimeMillis() & 0xfffffff), username, parts[3]));

                    InboundPeerConnection inboundPeerConnection = new InboundPeerConnection(s, 700, parts[3]);
                    Packet p = inboundPeerConnection.receiveMessage();

                    if (p instanceof AckMessage) {
                        System.out.println("Received ack message from " + p.getSender());
                        System.out.println("Connection established with " + p.getSender());
                        receiverThread.addInboundPeerConnection(inboundPeerConnection);
                        senderThread.addOutboundPeerConnection(outboundPeerConnection);
                    } else {
                        System.out.println("Unknown packet type received from " + p.getSender());
                        System.out.println("Closing connection...");
                        s.close();
                    }
                } else if (input.startsWith("/disconnect ")) {
                    String[] parts = input.split(" ");
                    if (parts.length != 2) {
                        System.out.println("Invalid command. Type /help for help.");
                        continue;
                    }

                    receiverThread.removeInboundPeerConnection(parts[1]);

                    outbound.put(new CloseConnectionMessage((int) (System.currentTimeMillis() & 0xfffffff), username, parts[1]));
                } else if (input.startsWith("/to")) {
                    String[] parts = input.split(" ");
                    if (parts.length < 3) {
                        System.out.println("Invalid command. Type /help for help.");
                        continue;
                    }
                    StringBuilder message = new StringBuilder();
                    for (int i = 2; i < parts.length; i++) {
                        message.append(parts[i]);
                        message.append(" ");
                    }
                    outbound.put(new Message((int) (System.currentTimeMillis() & 0xfffffff), username, parts[1], message.toString()));
                } else if (input.equals("/exit")) {
                    System.out.println("Disconnecting from all peers...");
                    outbound.put(new CloseConnectionMessage((int) (System.currentTimeMillis() & 0xfffffff), username, "*"));

                    while (true) {
                        if (outbound.isEmpty()) {
                            break;
                        }
                    }

                    System.out.println("Exiting...");
                    System.exit(0);
                } else if (input.equals("/help")) {
                    System.out.println("Type a message and press enter to send it to all connected peers.");
                    System.out.println("Type /connect <username> to connect to a peer.");
                    System.out.println("Type /disconnect <username> to disconnect from a peer.");
                    System.out.println("Type /exit to exit the chat.");
                    System.out.println("Type /help to see this message again.");
                } else {
                    outbound.put(new Message((int) (System.currentTimeMillis() & 0xfffffff), username, "*", input));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

package org.example;

import org.example.comms.handlers.MessageManagerThread;
import org.example.comms.handlers.NewConnectionListener;
import org.example.comms.handlers.ReceiverThread;
import org.example.comms.handlers.SenderThread;
import org.example.comms.messages.Packet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String[] args) {
        System.out.println("Username: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String username = reader.readLine();
            BlockingQueue<Packet> outbound = new LinkedBlockingQueue<>();
            BlockingQueue<Packet> inbound = new LinkedBlockingQueue<>();
            SenderThread senderThread = new SenderThread(outbound);
            ReceiverThread receiverThread = new ReceiverThread(inbound);
            NewConnectionListener newConnectionListener = new NewConnectionListener(username, receiverThread, senderThread);
            MessageManagerThread messageManagerThread = new MessageManagerThread(inbound, outbound, username, receiverThread, senderThread);

            Thread sThread = new Thread(senderThread);
            Thread rThread = new Thread(receiverThread);
            Thread nThread = new Thread(newConnectionListener);
            Thread mThread = new Thread(messageManagerThread);

            sThread.start();
            rThread.start();
            nThread.start();
            mThread.start();

            ConsoleInterface consoleInterface = new ConsoleInterface(outbound, receiverThread, senderThread, username, reader);
            consoleInterface.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}